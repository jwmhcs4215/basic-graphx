import org.apache.spark._
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD

val sc = new SparkContext("local", "GraphX Basic Test", new SparkConf())

val vertices: RDD[(VertexId, (String, String))] =
  sc.parallelize(Array((1L, ("Mary", "England")), (2L, ("John", "Singapore")),
    (3L, ("Ronny", "USA"))))

val edges: RDD[Edge[String]] =
  sc.parallelize(Array(Edge(1L, 2L, "daughter"), Edge(1L, 3L, "wife"),
    Edge(2L, 3L, "son")))

val default = ("Heather", "Random")

val graph = Graph(vertices, edges, default)

//Count all the vertices
println(s"!!!!!!! Number of Vertices")
graph.numVertices

//Count all the edges
println(s"!!!!!!! Number of Edges")
graph.numEdges

//Count the out going edges from each node
println(s"!!!!!!! Outdegree of nodes")
val degrees = graph.outDegrees
degrees.take(10)

//Count the in going edges from each node
println(s"!!!!!!! Indegree of nodes")
val degrees2 = graph.inDegrees
degrees2.take(10)

//gives you the node ID with the number of triangles next to that triangle
println(s"!!!!!!! Triangle count")
val tr = graph.triangleCount
tr.vertices.take(10)

// gives the connected components
println(s"!!!!!!! Connected components")
val connectedComp = graph.connectedComponents().vertices
connectedComp.take(10)

sc.stop()
System.exit(0)


// Interesting Trivia on how take works
// For example let's say you take(200), and each partition has 10 elements. 
// Take will first fetch partition 0 and see that it has 10 elements.
// It assumes that it would need 20 such partitions to get 200 elements. 
// But it's better to ask for a bit more in a parallel request. So it wants 30 partitions, and it already has 1. 
// So it fetches partitions 1 to 29 next, in parallel. 
// This will likely be the last step. 
// If it's very unlucky, and does not find a total of 200 elements, it will again make an estimate and request another batch in parallel.