# Basic GraphX

# Interesting Trivia on how take works
For example let's say you take(200), and each partition has 10 elements. 
Take will first fetch partition 0 and see that it has 10 elements.
It assumes that it would need 20 such partitions to get 200 elements. 
But it's better to ask for a bit more in a parallel request. So it wants 30 partitions, and it already has 1. 
So it fetches partitions 1 to 29 next, in parallel. 
This will likely be the last step. 
If it's very unlucky, and does not find a total of 200 elements, it will again make an estimate and request another batch in parallel.